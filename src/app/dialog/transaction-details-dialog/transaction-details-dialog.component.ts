import { Component, OnInit } from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {MatDialogRef} from '@angular/material';
import {DialogsActions} from '../../store/actions/dialogs.actions';

@Component({
  selector: 'app-transaction-details-dialog',
  templateUrl: './transaction-details-dialog.component.html',
  styleUrls: ['./transaction-details-dialog.component.scss']
})
export class TransactionDetailsDialogComponent implements OnInit {

  constructor(private ngRedux: NgRedux<AppState>,
              public dialogRef: MatDialogRef<TransactionDetailsDialogComponent>) {
  }

  closeDialog() {
    this.ngRedux.dispatch({type: DialogsActions.CLOSE_ALL_DIALOGS });
  }

  ngOnInit() {
    this.dialogRef.disableClose = true;
  }
}
