import { Component, OnInit } from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {MatDialogRef} from '@angular/material';
import {DialogsActions} from '../../store/actions/dialogs.actions';

@Component({
  selector: 'app-info-sharing-list-comment-dialog',
  templateUrl: './info-sharing-list-comment-dialog.component.html',
  styleUrls: ['./info-sharing-list-comment-dialog.component.scss']
})
export class InfoSharingListCommentDialogComponent implements OnInit {

  constructor(private ngRedux: NgRedux<AppState>,
              public dialogRef: MatDialogRef<InfoSharingListCommentDialogComponent>) {
  }

  closeDialog() {
    this.ngRedux.dispatch({type: DialogsActions.CLOSE_ALL_DIALOGS });
  }

  ngOnInit() {
    this.dialogRef.disableClose = true;
  }
}
