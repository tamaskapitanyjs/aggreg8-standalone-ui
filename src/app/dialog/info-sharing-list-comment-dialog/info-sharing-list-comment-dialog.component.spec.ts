import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InfoSharingListCommentDialogComponent } from './info-sharing-list-comment-dialog.component';

describe('InfoSharingListCommentDialogComponent', () => {
  let component: InfoSharingListCommentDialogComponent;
  let fixture: ComponentFixture<InfoSharingListCommentDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InfoSharingListCommentDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InfoSharingListCommentDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
