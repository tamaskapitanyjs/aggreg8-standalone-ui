import { Injectable } from '@angular/core';
import {MatDialog} from '@angular/material';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../store/root.state';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  supportedDialogs = {};

  constructor(public dialog: MatDialog,
              private ngRedux: NgRedux<AppState>) { }

  public openDialog(name: string, data?: any) {
    const dialogClass = this.supportedDialogs[name];

    if (data) {
      this.dialog.open(dialogClass, { data: data });
    } else {
      this.dialog.open(dialogClass, {});
    }
  }

  public registerDialog(name: string, type: any) {
    this.supportedDialogs[name] = type;
  }

  public closeDialog() {
    this.dialog.closeAll();
  }
}
