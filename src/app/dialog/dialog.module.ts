import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransactionDetailsDialogComponent} from './transaction-details-dialog/transaction-details-dialog.component';
import {InfoSharingListCommentDialogComponent} from './info-sharing-list-comment-dialog/info-sharing-list-comment-dialog.component';
import {SharedModule} from '../shared/shared.module';
import {TransactionsModule} from '../transactions/transactions.module';

@NgModule({
  declarations: [
    TransactionDetailsDialogComponent,
    InfoSharingListCommentDialogComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TransactionsModule
  ],
  entryComponents: [
    TransactionDetailsDialogComponent,
    InfoSharingListCommentDialogComponent
  ]
})
export class DialogModule { }
