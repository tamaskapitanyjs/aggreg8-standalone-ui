import { Component } from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from './store/root.state';
import {DialogService} from './dialog/dialog.service';
import {TransactionDetailsDialogComponent} from './dialog/transaction-details-dialog/transaction-details-dialog.component';
import {InfoSharingListCommentDialogComponent} from './dialog/info-sharing-list-comment-dialog/info-sharing-list-comment-dialog.component';
import {ModelActions} from './store/actions/model.actions';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private ngRedux: NgRedux<AppState>,
              private dialogService: DialogService) {
    this.registerDialogsToState();
    this.ngRedux.dispatch({ type: ModelActions.INIT_TRANSACTIONS_AND_CATEGORIES });
    this.ngRedux.dispatch({ type: ModelActions.FETCH_USER_DATA });
  }

  registerDialogsToState() {
    this.dialogService.registerDialog('TransactionDetailsDialogComponent', TransactionDetailsDialogComponent);
    this.dialogService.registerDialog('InfoSharingListCommentDialogComponent', InfoSharingListCommentDialogComponent);
  }
}
