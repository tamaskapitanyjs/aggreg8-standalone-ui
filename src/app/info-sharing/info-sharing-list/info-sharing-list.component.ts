import { Component, OnInit } from '@angular/core';
import {DialogsActions} from '../../store/actions/dialogs.actions';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../store/root.state';

@Component({
  selector: 'app-info-sharing-list',
  templateUrl: './info-sharing-list.component.html',
  styleUrls: ['./info-sharing-list.component.scss']
})
export class InfoSharingListComponent implements OnInit {

  infoSharingConsents = [];

  constructor(private ngRedux: NgRedux<AppState>) { }

  ngOnInit() {
  }

  showInfoSharingListComment() {
    this.ngRedux.dispatch({ type: DialogsActions.OPEN_DIALOG, payload: 'InfoSharingListCommentDialogComponent' });
  }
}
