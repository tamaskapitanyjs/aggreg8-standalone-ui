import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InfoSharingComponent } from './info-sharing.component';
import { InfoSharingListComponent } from './info-sharing-list/info-sharing-list.component';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  declarations: [
    InfoSharingComponent,
    InfoSharingListComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    InfoSharingComponent,
    InfoSharingListComponent,
  ]
})
export class InfoSharingModule { }
