import {Bank, BankAccessConsent, RawAccount, Transaction} from './model.state';

export interface ConnectToNewBank {
  selectedBank: Bank;
  syncInProgress: boolean;
  needsOTC: boolean;
  needsMobileToken: boolean;
  syncSpinnerProgressValue: number;
}

export enum VisibleStandaloneUiTabs {
  SYNC_TAB,
  TRANSACTIONS_TAB,
  SHARE_TAB,
  BALANCES_TAB
}

export interface SyncConsent {
  currentConsent: BankAccessConsent;
}

export interface EditConsent {
  editedConsent: BankAccessConsent;
  bank: Bank;
}

export enum VisibleComponentElements {
  NONE,
  CONNECT_NEW_BANK_COMPONENT,
  SYNC_BANK_ACCESS_CONSENT_COMPONENT,
  CONSENT_LIST_COMPONENT,
  CONSENT_UI_COMPONENT,
  ENTER_CREDENTIALS,
  EDIT_BANK_ACCESS_CONSENT,
  ERROR,
  TRANSACTION_LIST_COMPONENT,
  EDIT_TRANSACTIONS_FILTER_COMPONENT,
  INFO_SHARING_SELECTION_UI,
  INFO_SHARING_CONFIRM_UI,
  INFO_SHARING_LIST_UI,
  INFO_SHARING_EDIT_UI,
  EDIT_PROFILE,
  ADD_ACCOUNTS_COMPONENT
}

export interface TransactionsFilter {
  dateBeginFilter: Date;
  dateEndFilter: Date;
  filteredAccountList: Account[];
}

export interface Layout {
  connectToNewBank?: ConnectToNewBank;
  syncConsent?: SyncConsent;
  visibleComponent?: VisibleComponentElements;
  visibleAccounts?: RawAccount[];
  editConsent?: EditConsent;
  formattedTransactions?: Object[];
  emptyTransactions?: boolean;
  transactionsfilter?: TransactionsFilter;
  currentCheckedAccounts?: Object[];
  clusteredPermissions?: Map<String, Array<String>>;
  emptyConsentedAccounts?: boolean;
  newBankAccessConsentIds?: Array<String>;
  separatedConsentsForIscSelection?: any;
  currentCustomerName?: string;
  newConsentedAccountsIds?: Array<String>;
  isOnDemandSync?: boolean;
  activeTabInStandalone?: number;
  selectedTransaction?: Transaction;
  selectedInfoSharing?: any;
  authServerPasswordUpdateSuccess?: boolean;
  userDataUpdateSuccess?: boolean;
  sidenavClass?: string;
  sideNavIsOpen: boolean;
  visibleStandaloneUiTabs?: VisibleStandaloneUiTabs;
  selectedBACToRevokeInNonApisEmbeddedMode: BankAccessConsent;
  groupedAvailableAccounts?: Map<String, Array<Object>>;
  hasSelectedAccounts?: boolean;
  currentInfoSharingConsentId?: string;
}

export const INITIAL_LAYOUT_STATE = {
  connectToNewBank: null,
  syncConsent: null,
  visibleComponent: VisibleComponentElements.NONE,
  visibleAccounts: null,
  editConsent: null,
  formattedTransactions: null,
  emptyTransactions: null,
  transactionsFilter: null,
  currentCheckedAccounts: null,
  clusteredPermissions: null,
  emptyConsentedAccounts: null,
  newBankAccessConsentIds: [],
  newConsentedAccountsIds: [],
  separatedConsentsForIscSelection: null,
  currentCustomerName: null,
  isOnDemandSync: false,
  activeTabInStandalone: null,
  selectedTransaction: null,
  selectedInfoSharing: null,
  authServerPasswordUpdateSuccess: null,
  userDataUpdateSuccess: null,
  sidenavClass: null,
  sideNavIsOpen: false,
  visibleStandaloneUiTabs: null,
  selectedBACToRevokeInNonApisEmbeddedMode: null,
  groupedAvailableAccounts: null,
  hasSelectedAccounts: null,
  currentInfoSharingConsentId: null
};
