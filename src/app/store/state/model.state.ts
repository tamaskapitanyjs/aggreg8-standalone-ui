export enum FetchModelStatus {
  NONE,
  INIT_MODEL,
  FETCH_RESOURCE,
  FETCH_RESOURCE_SUCCESS,
  FETCH_RESOURCE_ERROR,
  BUILD_MODEL,
  BUILD_MODEL_SUCCESS,
  BUILD_MODEL_ERROR,
  UPDATE_MODEL_SUCCESS
}

export interface BankAccessConsent {
  _id?: string;
  userId?: string;
  bankId?: string;
  consentedAccounts?: Array<Account>;
  availableAccounts?: Array<RawAccount>;
  syncStatus?: string;
  error?: string;
  hasSavedCredentials: boolean;
  syncProgressValue: string;
  name: string;
}

export interface RawAccount {
  accountName?: string;
  currency?: string;
  client?: string;
  accountNumber?: string;
}

export interface Bank {
  _id?: string;
  name?: string;
  icon?: string;
  show?: boolean;
  average_scrape_time?: number;
  savings_type_keywords?: Object;
  available_countries?: Array<string>;
}

export interface Category {
  _id?: string;
  parent_id?: string;
  user?: string;
  global_category_id?: string;
  class_name?: string;
  color?: string;
  hidden?: boolean;
  deleted?: boolean;
  monthly_budget?: string;
  service_codes?: string;
  created?: string;
  name?: string;
}

export interface Customer {
  _id?: string;
  companyName?: string;
  dataScope?: Array<string>;
  aisp?: boolean;
  dataHandlingPurposes?: Array<string>;
  name?: string;
  contactInfo?: string;
}

export interface InfoSharingConsent {
  _id?: string;
  userId?: string;
  customer?: Customer;
  consentedAccounts?: Array<Account>;
  deleted?: boolean;
}

export enum UiModes {
  Standalone, AISP, NonAISPEmbedded, OIDC
}

export interface Model {
  bankAccessConsents?: BankAccessConsent[];
  banks?: Bank[];
  status?: FetchModelStatus;
  currentConsentId?: string;
  sessionToken?: string;
  transactions?: Transaction[];
  allTransactions?: Transaction[];
  categories?: Category[];
  customer?: Customer;
  authorizeError?: string;
  uiMode?: UiModes;
  infoSharingConsents?: InfoSharingConsent[];
  keycloakResourcesPath?: string;
  aggreg8?: Customer;
  userData?: any;
  currentSyncTaskId?: string;
}

export const INITIAL_MODEL_STATE = {
  bankAccessConsents: [],
  banks: [],
  status: FetchModelStatus.NONE,
  currentConsentId: null,
  sessionToken: null,
  transactions: null,
  allTransactions: null,
  categories: null,
  customer: null,
  uiMode: null,
  infoSharingConsents: [],
  keycloakResourcesPath: null,
  aggreg8: null,
  userData: null,
  currentSyncTaskId: null
};

export interface Transaction {
  _id?: string;
  createdAt?: string;
  updatedAt?: string;
  rawindex?: string;
  amount?: string;
  type?: string;
  comment?: boolean;
  client?: boolean;
  date?: string;
  account_id?: string;
  cardnumber?: string;
  user?: string;
  category_id?: string;
  scrapesession_id?: string;
  noservice?: string;
  deleted?: string;
  tags?: string;
  split?: string;
  created?: string;
  original_partner?: string;
  name?: string;
  is_daily_sum?: boolean;
  partner_id?: string;
  displayDate?: string;
}
