export interface Error {
  errorMsg?: string;
  bankAccessConsentSyncError?: string;
  syncError?: string;
  isError?: boolean;
}

// REFACTOR
export const INITIAL_ERRORS_STATE = {
  errorMsg: null,
  bankAccessConsentSyncError: null,
  syncError: null,
  isError: false
};
