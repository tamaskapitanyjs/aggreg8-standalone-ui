export class Dialog {
  openedDialogName: string;
  resultAfterPaymentObject: object;
}

export const INITIAL_DIALOGS_STATE = {
  openedDialogName: null,
  resultAfterPaymentObject: null
};
