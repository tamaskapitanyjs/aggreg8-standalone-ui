import { AppState } from './root.state';

import * as MODEL from './reducers/model.reducer';
import * as LAYOUT from './reducers/layout.reducer';
import * as DIALOG from './reducers/dialogs.reducer';
import * as ERROR from './reducers/errors.reducer';

import { combineReducers } from 'redux';

export const rootReducer = combineReducers<AppState>({
  model: MODEL.modelReducer,
  layout: LAYOUT.layoutReducer,
  dialog: DIALOG.dialogReducer,
  error: ERROR.errorReducer
});
