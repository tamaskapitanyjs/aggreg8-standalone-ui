import {Layout, INITIAL_LAYOUT_STATE} from '../state/layout.state';
import {PayloadAction, LayoutActions} from '../actions/layout.actions';


export function layoutReducer(state: Layout, action: PayloadAction): Layout {
  if (!state) {
    state = INITIAL_LAYOUT_STATE;
  }

  switch (action.type) {

    case LayoutActions.VISIBLE_COMPONENT:
      return {
        ...state,
        visibleComponent: action.payload
      };

    case LayoutActions.CHECK_EMPTY_BANK_ACCESS_CONSENTED_ACCOUNTS:
      return {
        ...state,
        emptyConsentedAccounts: null
      };

    case LayoutActions.EMPTY_BANK_ACCESS_CONSENTED_ACCOUNTS:
      return {
        ...state,
        emptyConsentedAccounts: true
      };

    case LayoutActions.CONTINUE_BANK_ACCESS_CONSENT:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank
        }
      };

    case LayoutActions.SELECT_BANK:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          selectedBank: action.payload
        },
        currentCheckedAccounts: null,
        hasSelectedAccounts: null
      };

    case LayoutActions.NEEDS_OTC:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          needsOTC: true
        },
        currentCheckedAccounts: null
      };

    case LayoutActions.NEEDS_MOBILE_TOKEN:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          needsMobileToken: true
        },
        currentCheckedAccounts: null
      };

    case LayoutActions.REMOVE_NEEDS_OTC_STATE:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          needsOTC: false
        }
      };

    case LayoutActions.REMOVE_NEEDS_MOBILE_TOKEN_STATE:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          needsMobileToken: false
        }
      };

    case LayoutActions.BANK_ACCESS_CONSENT_SCRAPE_ACCOUNTS_AVAILABLE:
      return {
        ...state,
        visibleAccounts: action.payload.availableAccounts,
        connectToNewBank: {
          ...state.connectToNewBank
        },
      };

    case LayoutActions.SEPARATED_BAC_CONSENTED_ACCOUNTS_FOR_ISC_SELECTION:
      return {
        ...state,
        separatedConsentsForIscSelection: action.payload
      };

    case LayoutActions.SET_BANK_ACCESS_CONSENT_FOR_SYNC:
      return {
        ...state,
        syncConsent: {
          ...state.syncConsent,
          currentConsent: action.payload,

        },
        currentCheckedAccounts: null
      };

    case LayoutActions.SET_BANK_FOR_SYNC:
      return {
        ...state,
        syncConsent: {
          ...state.syncConsent
        },
        currentCheckedAccounts: null
      };

    case LayoutActions.SET_ON_DEMAND_SYNC_TO_TRUE:
      return {
        ...state,
        isOnDemandSync: true
      };
    case LayoutActions.SYNC_COMPLETE:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          syncInProgress: false,
        },
        isOnDemandSync: null,
        hasSelectedAccounts: null,
        groupedAvailableAccounts: null
      };

    case LayoutActions.ADD_BAC_IDS_TO_NEW_ARRAY:
      return {
        ...state,
        newBankAccessConsentIds: action.payload
      };

    case LayoutActions.ADD_CONSENTED_ACCOUNT_IDS_TO_NEW_ARAY:
      return {
        ...state,
        newConsentedAccountsIds: action.payload
      };

    case LayoutActions.GO_TO_SYNC_BAC:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          syncInProgress: true,
        }
      };

    case LayoutActions.ADD_CURRENT_CUSTOMER_NAME:
      return {
        ...state,
        currentCustomerName: action.payload
      };

    case LayoutActions.REMOVE_SYNC_BANK_ACCESS_CONSENT_STATE:
      return {
        ...state,
        syncConsent: {
          ...state.syncConsent,
          currentConsent: null
        },
        connectToNewBank: {
          selectedBank: null,
          syncInProgress: false,
          needsOTC: false,
          needsMobileToken: false,
          syncSpinnerProgressValue: null
        }
      };

    case LayoutActions.EDIT_BANK_ACCESS_CONSENT:
      return {
        ...state,
        editConsent: {
          ...state.editConsent,
          editedConsent: action.payload.bankAccessConsent,
          bank: action.payload.bank
        }
      };

    case LayoutActions.REMOVE_EDIT_BANK_ACCESS_CONSENT_STATE:
      return {
        ...state,
        editConsent: null
      };

    case LayoutActions.FORMATTED_TRANSACTIONS:
      return {
        ...state,
        formattedTransactions: action.payload,
        emptyTransactions: false
      };

    case LayoutActions.EMPTY_FORMATTED_TRANSACTIONS:
      return {
        ...state,
        emptyTransactions: true
      };

    case LayoutActions.REFRESH_FILTERED_ACCOUNTS_LIST:
      return {
        ...state,
        transactionsfilter: {
          ...state.transactionsfilter,
          filteredAccountList: action.payload
        }
      };
    case LayoutActions.TOGGLE_SIDENAV:
      console.log('CICA', action.payload);
      return {
        ...state,
        sideNavIsOpen: action.payload
    };

    case LayoutActions.CLEAR_FILTERED_ACCOUNT_LIST:
      return {
        ...state,
        transactionsfilter: {
          ...state.transactionsfilter,
          filteredAccountList: null
        }
      };

    case LayoutActions.CURRENT_CHECKED_ACCOUNTS:
      return {
        ...state,
        currentCheckedAccounts: action.payload
      };

    case LayoutActions.CLUSTERED_PERMISSIONS:
      return {
        ...state,
        clusteredPermissions: action.payload
      };

    case LayoutActions.MODIFY_TRANSACTION_BEGIN_DATE:
      return {
        ...state,
        transactionsfilter: {
          ...state.transactionsfilter,
          dateBeginFilter: action.payload.dateBeginFilter,
          dateEndFilter: action.payload.dateEndFilter
        }
      };

    case LayoutActions.MODIFY_TRANSACTION_END_DATE:
      return {
        ...state,
        transactionsfilter: {
          ...state.transactionsfilter,
          dateEndFilter: action.payload
        }
      };

    case LayoutActions.SET_ACTIVE_TAB_IN_STANDALONE:
      return {
        ...state,
        activeTabInStandalone: action.payload
      };

    case LayoutActions.SHOW_SELECTED_TRANSACTION_DETAIL:
      return {
        ...state,
        selectedTransaction: action.payload
      };

    case LayoutActions.OPEN_SELECTED_INFO_SHARING_CONSENT:
      return {
        ...state,
        selectedInfoSharing: action.payload
      };

    case LayoutActions.REMOVE_SELECTED_INFO_SHARING_CONSENT_STATE:
      return {
        ...state,
        selectedInfoSharing: null
      };

    case LayoutActions.REMOVE_UPDATE_BANK_STATE:
      return {
        ...state,
        isOnDemandSync: null,
        groupedAvailableAccounts: null
      };

    case LayoutActions.REMOVE_IS_ONDEMAND_SYNC_STATUS:
      return {
        ...state,
        isOnDemandSync: null
      };

    case LayoutActions.UPDATE_AUTH_SERVER_PASSWORD_SUCCESS:
      return {
        ...state,
        authServerPasswordUpdateSuccess: action.payload
      };

    case LayoutActions.UPDATE_USER_DATA_SUCCESS:
      return {
        ...state,
        userDataUpdateSuccess: action.payload
      };

    case LayoutActions.DELETE_IS_SUCCESSFUL_USER_UPDATE_STATE:
      return {
        ...state,
        userDataUpdateSuccess: null,
        authServerPasswordUpdateSuccess: null
      };

    case LayoutActions.SIDENAV_CLASS:
      return {
        ...state,
        sidenavClass: action.payload
      };

    case LayoutActions.SYNC_PROGRESS_SPINNER_VALUE:
      return {
        ...state,
        connectToNewBank: {
          ...state.connectToNewBank,
          syncSpinnerProgressValue: action.payload
        }
      };
    case LayoutActions.REVOKE_BAC_SHARING_IN_NONAIPS_MODE:
      return {
        ...state,
        selectedBACToRevokeInNonApisEmbeddedMode: action.payload
      };
    case LayoutActions.STORE_GROUPPED_AVAILABLE_ACCOUNTS_IN_AISP_UIMODE:
      return {
        ...state,
        groupedAvailableAccounts: action.payload
      };
    case LayoutActions.ADD_SELECTED_ACCOUNTS:
      return {
        ...state,
        hasSelectedAccounts: true
      };
    case LayoutActions.START_SHARE_SUCCESS:
      return {
        ...state,
        currentInfoSharingConsentId: action.payload
      };

    default:
      return state;
  }
}
