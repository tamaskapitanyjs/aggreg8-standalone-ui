import {Dialog, INITIAL_DIALOGS_STATE} from '../state/dialogs.state';
import {PayloadAction} from '../actions/model.actions';
import {DialogsActions} from '../actions/dialogs.actions';

export function dialogReducer(state: Dialog, action: PayloadAction): Dialog {
  if (!state) {
    state = INITIAL_DIALOGS_STATE;
  }

  switch (action.type) {
    case DialogsActions.OPEN_DIALOG: {
      return {
        ...state,
        openedDialogName: action.payload
      };
    }
    case DialogsActions.RESET_DIALOG_STATE: {
      return {
        openedDialogName: null,
        resultAfterPaymentObject: null
      };
    }
    default:
      return state;
  }
}
