import {PayloadAction} from '../actions/model.actions';
import {INITIAL_ERRORS_STATE, Error} from '../state/error.state';
import {ErrorActions} from '../actions/error.actions';

export function errorReducer(state: Error, action: PayloadAction): Error {
  if (!state) {
    state = INITIAL_ERRORS_STATE;
  }

  switch (action.type) {
    case ErrorActions.INIT_MODEL_ERROR:
      return {
        ...state,
        isError: true,
        errorMsg: action.payload
      };
    default:
      return state;
  }
}
