import { Model, INITIAL_MODEL_STATE, FetchModelStatus } from '../state/model.state';
import { PayloadAction, ModelActions } from '../actions/model.actions';

export function modelReducer(state: Model, action: PayloadAction): Model {
  if (!state) {
    state = INITIAL_MODEL_STATE;
  }

  switch (action.type) {

    case ModelActions.INIT_MODEL:
      return {
        ...state,
        status: FetchModelStatus.INIT_MODEL
      };

    case ModelActions.ADD_SYNC_SESSION_TOKEN_TO_MODEL:
      return {
        ...state,
        sessionToken: action.payload
      };

    case ModelActions.INIT_MODEL_SUCCESS:
      return {
        ...state,
        bankAccessConsents: action.payload.bankAccessConsents,
        banks: action.payload.banks,
        aggreg8: action.payload.aggreg8,
        status: FetchModelStatus.BUILD_MODEL_SUCCESS
      };

    case ModelActions.INIT_TRANSACTIONS_AND_CATEGORIES_SUCCESS:
      return {
        ...state,
        transactions: action.payload.transactions,
        allTransactions: action.payload.transactions,
        categories: action.payload.categories
      };

    case ModelActions.SET_CURRENT_BANK_ACCESS_CONSENT_ID:
      return {
        ...state,
        currentConsentId: action.payload
      };

    case ModelActions.SET_CURRENT_CUSTOMER:
      return {
        ...state,
        customer: action.payload
      };

    case ModelActions.SET_UI_MODE:
      return {
        ...state,
        uiMode: action.payload
      };

    case ModelActions.FILTERED_TRANSACTIONS:
      return {
        ...state,
        transactions: action.payload
      };

    case ModelActions.SET_INFO_SHARING_CONSENTS_MODEL:
      return {
        ...state,
        infoSharingConsents: action.payload
      };

    case ModelActions.SET_KEYCLOAK_RESOURCES_PATH:
      return {
        ...state,
        keycloakResourcesPath: action.payload
      };

    case ModelActions.MODIFY_TRANSACTION_LIST_BY_DATE:
      return {
        ...state,
        transactions: action.payload
      }
    case ModelActions.MODIFY_TRANSACTION_LIST_BY_ACCOUNT:
      return {
        ...state,
        transactions: action.payload
      };

    case ModelActions.ADD_CUSTOMER_TEMPORARILY:
      return {
        ...state,
        customer: action.payload
      };

    case ModelActions.REMOVE_SELECTED_CUSTOMER_IN_ISC_SELECTION_FROM_STATE:
      return {
        ...state,
        customer: null
      };

    case ModelActions.REVOKE_CURRENT_BAC_ID:
      return {
        ...state,
        currentConsentId: null
      };

    case ModelActions.FETCH_USER_DATA_SUCCESS:
      return {
        ...state,
        userData: action.payload
      };

    case ModelActions.SET_CURRENT_SYNCTASK_ID:
      return {
        ...state,
        currentSyncTaskId: action.payload
      };

    case ModelActions.FETCH_ISCS_BEFORE_START_SHARE_SUCCESS:
      return {
        ...state,
        infoSharingConsents: action.payload
      };

    default:
      return state;
  }
}
