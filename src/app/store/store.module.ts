import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DevToolsExtension, NgRedux, NgReduxModule} from '@angular-redux/store';
import {AppState, INITIAL_STATE} from './root.state';
import {rootReducer} from './root.reducer';
import {EpicCollector} from './epics';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    NgReduxModule
  ],
})
export class StoreModule {
  constructor(store: NgRedux<AppState>,
              private epicCollector: EpicCollector,
              private devTools: DevToolsExtension) {

    let enhancers = [];
    const middleware = this.epicCollector.getMiddlewares();

    // You probably only want to expose this tool in devMode.
    if (devTools.isEnabled()) {
      enhancers = [...enhancers, devTools.enhancer()];
    }

    store.configureStore(
      rootReducer,
      INITIAL_STATE,
      middleware,
      enhancers);
  }
}
