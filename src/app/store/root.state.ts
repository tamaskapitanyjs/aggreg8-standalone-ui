import {INITIAL_MODEL_STATE, Model} from './state/model.state';
import {INITIAL_LAYOUT_STATE, Layout} from './state/layout.state';
import {Dialog, INITIAL_DIALOGS_STATE} from './state/dialogs.state';
import {Error, INITIAL_ERRORS_STATE} from './state/error.state';

export interface AppState {
  model?: Model;
  layout?: Layout;
  dialog?: Dialog;
  error?: Error;
}

export const INITIAL_STATE = {
  model: INITIAL_MODEL_STATE,
  layout: INITIAL_LAYOUT_STATE,
  dialog: INITIAL_DIALOGS_STATE,
  error: INITIAL_ERRORS_STATE
};
