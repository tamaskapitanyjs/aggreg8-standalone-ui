import { EpicCollector } from './epic.collector';
import { LayoutEpics } from './layout.epics';
import { DialogEpics } from './dialogs.epics';
import {TransactionService} from '../../transactions/transaction.service';
import {DialogService} from '../../dialog/dialog.service';
import {ModelEpics} from './model.epics';
import {ErrorEpics} from './errors.epics';

export const EPIC_PROVIDERS = [ModelEpics, ErrorEpics, EpicCollector, LayoutEpics, DialogEpics];
export { ErrorEpics, ModelEpics, EpicCollector, LayoutEpics, DialogEpics };

export const SERVICE_PROVIDERS = [ TransactionService, DialogService];
export { TransactionService, DialogService };
