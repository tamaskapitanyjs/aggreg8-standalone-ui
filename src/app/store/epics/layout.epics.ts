import {NgRedux} from '@angular-redux/store';
import {Injectable} from '@angular/core';
import {ActionsObservable} from 'redux-observable';
import {Observable, of} from 'rxjs';
import {mergeMap} from 'rxjs/operators';
import {LayoutActions, PayloadAction} from '../actions/layout.actions';
import {AppState} from '../root.state';
import {DialogsActions} from '../actions/dialogs.actions';
import {TransactionService} from '../../transactions/transaction.service';
import {VisibleComponentElements} from '../state/layout.state';
import {ModelActions} from '../actions/model.actions';

@Injectable()
export class LayoutEpics {
  constructor(
    private ngRedux: NgRedux<AppState>,
    private transactionService: TransactionService) {
  }

  public showSelectedTransaction(action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> {
    return action$.ofType(LayoutActions.SHOW_SELECTED_TRANSACTION_DETAIL)
      .pipe(mergeMap(({payload}) => {
        return of({
          type: DialogsActions.OPEN_DIALOG, payload: 'TransactionDetailsDialogComponent'
        });
      }));
  }

  public formattingTransactions = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(LayoutActions.FORMAT_TRANSACTIONS)
      .pipe(mergeMap(({ payload }) => {
        if (this.ngRedux.getState().model.transactions && this.ngRedux.getState().model.transactions.length !== 0) {
          const formattingTransactions = this.transactionService.createDailySumTransactions();
          return of({
            type: LayoutActions.FORMATTED_TRANSACTIONS,
            payload: formattingTransactions
          });
        }
        return of({
          type: LayoutActions.EMPTY_FORMATTED_TRANSACTIONS
        });
      }));
  }

  public toggleSideNav = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(LayoutActions.TOGGLE_SIDENAV)
      .pipe(mergeMap(({ payload }) => {
        const sideNavIsOpen = this.ngRedux.getState().layout.sideNavIsOpen;
        if (sideNavIsOpen) {
          return of({
            type: null,
            payload: payload
          });
        }

        if (this.ngRedux.getState().error.errorMsg) {
          return of({
            type: LayoutActions.VISIBLE_COMPONENT,
            payload: VisibleComponentElements.ERROR
          });
        }
    }));
  }

  public gotToTransactionList = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(LayoutActions.FORMATTED_TRANSACTIONS)
      .pipe(mergeMap(({ payload }) => {
        if (this.ngRedux.getState().layout.activeTabInStandalone === 1) {
          return of({
            type: LayoutActions.VISIBLE_COMPONENT,
            payload: VisibleComponentElements.TRANSACTION_LIST_COMPONENT
          });
        }

        if (this.ngRedux.getState().error.errorMsg) {
          return of({
            type: LayoutActions.VISIBLE_COMPONENT,
            payload: VisibleComponentElements.ERROR
          });
        }


        return of({
          type: LayoutActions.CHECK_EMPTY_BANK_ACCESS_CONSENTED_ACCOUNTS
        });
      }));
  }

  public refreshFormattedTransactions = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(ModelActions.MODIFY_TRANSACTION_LIST_BY_ACCOUNT)
      .pipe(mergeMap(({ type, payload }) => {
        return of({
          type: LayoutActions.FORMAT_TRANSACTIONS
        });
      }));
  }
}
