import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import { ActionsObservable } from 'redux-observable';
import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { ModelActions, PayloadAction } from '../actions/model.actions';
import {AppState} from '../root.state';
import {TransactionService} from '../../transactions/transaction.service';
import {ErrorConverterService} from '../../core/services/error-converter.service';
import {ErrorActions} from '../actions/error.actions';
import {ModelService} from '../../core/services/model.service';
import {LayoutActions} from '../actions/layout.actions';
import {ApiService} from '../../core/services/api.service';

@Injectable()
export class ModelEpics {

    constructor(
        private transactionService: TransactionService,
        private ngRedux: NgRedux<AppState>,
        private errorConverterService: ErrorConverterService,
        private modelService: ModelService,
        private api: ApiService) { }

  public initTransactionsAndCategories = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(ModelActions.INIT_TRANSACTIONS_AND_CATEGORIES)
      .pipe(mergeMap(({ payload }) => {
        return of({
          type: ModelActions.FETCH_TRANSACTIONS_AND_CATEGORIES,
          payload: payload
        });
      }));
  }

  public fetchTransactionsAndCategories = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(ModelActions.FETCH_TRANSACTIONS_AND_CATEGORIES).pipe(mergeMap(({ payload }) => {
      return this.modelService.getTransactionsAndCategories().pipe(map((results) => {
        const transactionsAndCategories = {
          transactions: results[0],
          categories: results[1],
        };
        return {
          type: ModelActions.FETCH_TRANSACTIONS_AND_CATEGORIES_SUCCESS,
          payload: transactionsAndCategories
        };
      }));
    })).pipe(catchError((err) => {
      const error = this.errorConverterService.convertErrorToTranslate(err);
      return of({
        type: ErrorActions.FETCH_TRANSACTIONS_AND_CATEGORIES_ERROR,
        payload: error
      });
    }));
  }

  public initTransactionsAndCategoriesSuccess = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(ModelActions.FETCH_TRANSACTIONS_AND_CATEGORIES_SUCCESS)
      .pipe(mergeMap(({ payload }) => {
        return of({
          type: ModelActions.INIT_TRANSACTIONS_AND_CATEGORIES_SUCCESS,
          payload: payload
        });
      }));
  }

  public formatTransactions = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(ModelActions.INIT_TRANSACTIONS_AND_CATEGORIES_SUCCESS)
      .pipe(mergeMap(({ type, payload }) => {
        return of({
          type: LayoutActions.FORMAT_TRANSACTIONS
        });
      }));
  }

  public getUserData = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(ModelActions.FETCH_USER_DATA)
      .pipe(mergeMap(({ payload }) => {
        return this.api.getUserData().pipe(map((results) => {
          return {
            type: ModelActions.FETCH_USER_DATA_SUCCESS,
            payload: results
          };
        }));
      })).pipe(catchError((err) => {
        const error = this.errorConverterService.convertErrorToTranslate(err);
        return of({
          type: ErrorActions.FETCH_USER_DATA_ERROR,
          payload: error
        });
      }));
  }
}
