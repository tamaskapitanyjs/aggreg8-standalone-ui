import { NgRedux } from '@angular-redux/store';
import { Injectable } from '@angular/core';
import {ErrorConverterService} from '../../core/services/error-converter.service';
import {AppState} from '../root.state';

@Injectable()
export class ErrorEpics {
    constructor(
        private ngRedux: NgRedux<AppState>,
        private errorConverterService: ErrorConverterService) { }
}
