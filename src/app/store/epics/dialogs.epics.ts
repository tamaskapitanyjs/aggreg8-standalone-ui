import { Injectable } from '@angular/core';
import { ActionsObservable } from 'redux-observable';
import { Observable, of } from 'rxjs';
import { PayloadAction } from '../actions/layout.actions';
import { mergeMap } from 'rxjs/operators';
import {DialogsActions} from '../actions/dialogs.actions';
import {DialogService} from '../../dialog/dialog.service';

@Injectable()
export class DialogEpics {
  constructor(private dialogService: DialogService) { }

  public openDialog = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
      return action$.ofType(DialogsActions.OPEN_DIALOG)
          .pipe(mergeMap(({ payload }) => {
              if (payload.data) {
                  this.dialogService.openDialog(payload.name, payload.data);
              } else {
                  this.dialogService.openDialog(payload);
              }
              return of<PayloadAction>();
          }));
  }
  public closeDialogAndResetOpenDialog = (action$: ActionsObservable<PayloadAction>): Observable<PayloadAction> => {
    return action$.ofType(DialogsActions.CLOSE_ALL_DIALOGS)
      .pipe(mergeMap(({ payload }) => {
        this.dialogService.closeDialog();
        return of({
          type: DialogsActions.RESET_DIALOG_STATE
        });
      }));
  }
}
