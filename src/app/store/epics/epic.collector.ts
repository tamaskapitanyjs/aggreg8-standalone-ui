import { Injectable } from '@angular/core';
import { createEpicMiddleware } from 'redux-observable';
import { createLogger } from 'redux-logger';
import { LayoutEpics } from './layout.epics';
import {DialogEpics} from './dialogs.epics';
import {ModelEpics} from './model.epics';


@Injectable()
export class EpicCollector {
    constructor(
        private layoutEpics: LayoutEpics,
        private dialogEpics: DialogEpics,
        private modelEpics: ModelEpics,
    ) { }
    public getMiddlewares() {
      const middleware = [];

      const epics = [
        this.layoutEpics.showSelectedTransaction,
        this.layoutEpics.gotToTransactionList,
        this.layoutEpics.formattingTransactions,
        this.layoutEpics.refreshFormattedTransactions,
        this.layoutEpics.toggleSideNav,
        this.dialogEpics.openDialog,
        this.dialogEpics.closeDialogAndResetOpenDialog,
        this.modelEpics.fetchTransactionsAndCategories,
        this.modelEpics.formatTransactions,
        this.modelEpics.initTransactionsAndCategories,
        this.modelEpics.initTransactionsAndCategoriesSuccess,
        this.modelEpics.getUserData
    ];

      epics.forEach((epic) => {
        middleware.push(createEpicMiddleware(epic));
      });

      middleware.push(createLogger());
        return middleware;
    }
}
