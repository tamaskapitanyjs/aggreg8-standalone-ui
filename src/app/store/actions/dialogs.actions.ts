import {Injectable} from '@angular/core';

@Injectable()
export class DialogsActions {
  static readonly OPEN_DIALOG = 'OPEN_DIALOG';
  static readonly CLOSE_ALL_DIALOGS = 'CLOSE_ALL_DIALOGS';
  static readonly RESET_DIALOG_STATE = 'RESET_DIALOG_STATE';
}
