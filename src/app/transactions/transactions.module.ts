import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {TransactionsComponent} from './transactions.component';
import {TransactionListComponent} from './transaction-list/transaction-list.component';
import {SharedModule} from '../shared/shared.module';
import { TransactionDetailsComponent } from './transaction-list/transaction-details/transaction-details.component';

@NgModule({
  declarations: [
    TransactionsComponent,
    TransactionListComponent,
    TransactionDetailsComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    TransactionDetailsComponent,
    TransactionsComponent,
    TransactionListComponent
  ]
})
export class TransactionsModule { }
