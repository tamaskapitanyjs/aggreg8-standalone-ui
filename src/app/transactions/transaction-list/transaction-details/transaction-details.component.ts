import { Component, OnInit } from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {Transaction} from '../../../store/state/model.state';
import {AppState} from '../../../store/root.state';
import {TransactionService} from '../../transaction.service';
import {DialogsActions} from '../../../store/actions/dialogs.actions';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-transaction-details',
  templateUrl: './transaction-details.component.html',
  styleUrls: ['./transaction-details.component.scss']
})
export class TransactionDetailsComponent implements OnInit {

  @select(['layout', 'selectedTransaction'])
  selectedTransaction: Observable<Transaction>;

  constructor(private ngRedux: NgRedux<AppState>,
              public transactionService: TransactionService,
              public translateService: TranslateService) { }

  ngOnInit() {
  }

  closeDialog() {
    this.ngRedux.dispatch({type: DialogsActions.CLOSE_ALL_DIALOGS });
  }
}
