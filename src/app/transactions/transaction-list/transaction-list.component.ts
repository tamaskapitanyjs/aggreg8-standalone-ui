import { Component, OnInit } from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {LayoutActions} from '../../store/actions/layout.actions';
import {TransactionService} from '../transaction.service';
import {Observable} from 'rxjs';
import {Category, Transaction} from '../../store/state/model.state';
import {TranslateService} from '@ngx-translate/core';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html',
  styleUrls: ['./transaction-list.component.scss']
})
export class TransactionListComponent implements OnInit {

  @select(['model', 'transactions'])
  transactions: Observable<Transaction[]>;
  @select(['model', 'categories'])
  categories: Observable<Category[]>;
  @select(['layout', 'formattedTransactions'])
  formattedTransactions: Observable<Object[]>;
  @select(['layout', 'emptyTransactions'])
  emptyTransactions: Observable<Boolean>;
  @select(['layout', 'selectedTransaction'])
  selectedTransaction: Observable<Transaction>;

  keys = Object.keys;

  constructor(private ngRedux: NgRedux<AppState>,
              public  transactionService: TransactionService,
              public translateService: TranslateService) { }

  ngOnInit() {
    window.scrollTo(0, 0);
  }

  showTransactionDetails(selectedTransaction) {
     this.ngRedux.dispatch({
       type: LayoutActions.SHOW_SELECTED_TRANSACTION_DETAIL,
       payload: selectedTransaction
     });
  }
}
