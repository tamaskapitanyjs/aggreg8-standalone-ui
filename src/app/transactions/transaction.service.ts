import { Injectable } from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../store/root.state';
import {Category, Transaction} from '../store/state/model.state';
import * as moment from 'moment';
import {mockTransactions} from './transactions.mock.data';

@Injectable({
  providedIn: 'root'
})
export class TransactionService {

  constructor(private ngRedux: NgRedux<AppState>) { }

  addCategoryClassName(transaction: Transaction) {
    const categories = this.ngRedux.getState().model.categories;
    let categoryClassName = '';
    let categoryColor = '';
    let categoryName = '';
    for (const category of categories) {
      if (transaction.category_id === category._id) {
        if (category.class_name) {
          categoryClassName = category.class_name;
        } else {
          const transactionCategory = this.recursiveCategory(category.parent_id, categories);
          categoryClassName = transactionCategory.class_name;
        }
        if (category.color) {
          categoryColor = category.color;
        } else {
          const transactionCategory = this.recursiveCategoryColor(category.parent_id, categories);
          categoryColor = transactionCategory.color;
        }
        categoryName = category.name;
      }
    }

    return {
      categoryClassName: categoryClassName,
      categoryColor: categoryColor,
      categoryName: categoryName
    };
  }

  recursiveCategory(parent_id: string, categories: Category[]) {
    for (const category of categories) {
      if (category._id === parent_id) {
        if (!category.class_name) {
          this.recursiveCategory(parent_id, categories);
        } else {
          return category;
        }
      }
    }
  }

  recursiveCategoryColor(parent_id, categories) {

    for (const category of categories) {
      if (category._id === parent_id) {
        if (!category.color) {
          this.recursiveCategoryColor(parent_id, categories);
        } else {
          return category;
        }
      }
    }
  }

  public createDailySumTransactions() {
    const transactions = this.ngRedux.getState().model.transactions

    moment.locale('hu')
    const days = [];
    for (const transaction of transactions) {

      const formatDays = moment(transaction.date).format('L dddd');
      days[formatDays] = days[formatDays] || { sum: 0, transactions: [] };
      days[formatDays].transactions.push(transaction);
    }
    for (const day of Object.keys(days)) {
      const current = days[day];
      for (const transaction of current.transactions) {
        current.sum += transaction.amount;
      }
    }
    return days;
  }
}
