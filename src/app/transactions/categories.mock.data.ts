export const mockCategories: any = [{
  '_id': '58bfc0fcb150e07f1374d1b0',
  'parent_id': 'root',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010000',
  'class_name': 'category-expense',
  'color': '#D32F2F',
  'root': 'root',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.062Z',
  'name': 'Expense'
}, {
  '_id': '58bfc0fcb150e07f1374d1b1',
  'parent_id': 'root',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000020000',
  'class_name': 'category-income',
  'color': '#8BC34A',
  'root': 'root',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.064Z',
  'name': 'Income'
}, {
  '_id': '58bfc0fcb150e07f1374d1b2',
  'parent_id': 'root',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000030000',
  'class_name': 'category-transfer',
  'color': '#03A9F4',
  'root': 'root',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.065Z',
  'name': 'Transfer'
}, {
  '_id': '58bfc0fcb150e07f1374d1b3',
  'parent_id': '58bfc0fcb150e07f1374d1b1',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000020300',
  'class_name': 'category-scholarship',
  'color': '#00BCD4',
  'root': '58bfc0fcb150e07f1374d1b1',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.187Z',
  'name': 'TXT_INITVALUES_CATEGORY_SCHOLARSHIP'
}, {
  '_id': '58bfc0fcb150e07f1374d1b4',
  'parent_id': '58bfc0fcb150e07f1374d1b1',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000020100',
  'class_name': 'category-salary',
  'color': '#006C5C',
  'root': '58bfc0fcb150e07f1374d1b1',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.192Z',
  'name': 'TXT_INITVALUES_CATEGORY_SALARY'
}, {
  '_id': '58bfc0fcb150e07f1374d1b5',
  'parent_id': '58bfc0fcb150e07f1374d1b1',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000020400',
  'class_name': 'category-gift-received',
  'color': '#E91E63',
  'root': '58bfc0fcb150e07f1374d1b1',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.193Z',
  'name': 'TXT_INITVALUES_CATEGORY_GIFT_RECEIVED'
}, {
  '_id': '58bfc0fcb150e07f1374d1b6',
  'parent_id': '58bfc0fcb150e07f1374d1b1',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000020500',
  'class_name': 'category-loan-income',
  'color': '#CDDC39',
  'root': '58bfc0fcb150e07f1374d1b1',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.271Z',
  'name': 'TXT_INITVALUES_CATEGORY_LOAN_INCOME'
}, {
  '_id': '58bfc0fcb150e07f1374d1b7',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010100',
  'class_name': 'category-home',
  'color': '#0ac545',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.272Z',
  'name': 'TXT_INITVALUES_CATEGORY_LIVING_COSTS'
}, {
  '_id': '58bfc0fcb150e07f1374d1b8',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010300',
  'class_name': 'category-groceries',
  'color': '#f87b28',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.273Z',
  'name': 'TXT_INITVALUES_CATEGORY_FOOD'
}, {
  '_id': '58bfc0fcb150e07f1374d1b9',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010400',
  'class_name': 'category-fun',
  'color': '#34c3d5',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.273Z',
  'name': 'TXT_INITVALUES_CATEGORY_FUN'
}, {
  '_id': '58bfc0fcb150e07f1374d1ba',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000011100',
  'class_name': 'category-work-business',
  'color': '#CFCCA4',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.282Z',
  'name': 'TXT_INITVALUES_CATEGORY_WORK_BUSINESS'
}, {
  '_id': '58bfc0fcb150e07f1374d1bb',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010800',
  'class_name': 'category-education',
  'color': '#5bbc46',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.283Z',
  'name': 'TXT_INITVALUES_CATEGORY_EDUCATION'
}, {
  '_id': '58bfc0fcb150e07f1374d1bc',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010200',
  'class_name': 'category-household',
  'color': '#00aca1',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.392Z',
  'name': 'TXT_INITVALUES_CATEGORY_HOUSEHOLD'
}, {
  '_id': '58bfc0fcb150e07f1374d1bd',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000011000',
  'class_name': 'category-financial',
  'color': '#673AB7',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.395Z',
  'name': 'TXT_INITVALUES_CATEGORY_FINANCIAL'
}, {
  '_id': '58bfc0fcb150e07f1374d1be',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010700',
  'class_name': 'category-healthbeauty',
  'color': '#c55296',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.442Z',
  'name': 'TXT_INITVALUES_CATEGORY_HEALTH_BEAUTY'
}, {
  '_id': '58bfc0fcb150e07f1374d1bf',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010600',
  'class_name': 'category-shopping',
  'color': '#fbd200',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.443Z',
  'name': 'TXT_INITVALUES_CATEGORY_SHOPPING'
}, {
  '_id': '58bfc0fcb150e07f1374d1c0',
  'parent_id': '58bfc0fcb150e07f1374d1bd',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000011001',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.740Z',
  'name': 'TXT_INITVALUES_CATEGORY_BANK_CHARGES'
}, {
  '_id': '58bfc0fcb150e07f1374d1c1',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010606',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.762Z',
  'name': 'TXT_INITVALUES_CATEGORY_GIFTS'
}, {
  '_id': '58bfc0fcb150e07f1374d1c2',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010602',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.763Z',
  'name': 'TXT_INITVALUES_CATEGORY_CLOTHING'
}, {
  '_id': '58bfc0fcb150e07f1374d1c3',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010603',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.764Z',
  'name': 'TXT_INITVALUES_CATEGORY_ELECTRONIC_GADGETS'
}, {
  '_id': '58bfc0fcb150e07f1374d1c4',
  'parent_id': '58bfc0fcb150e07f1374d1bc',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010204',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.783Z',
  'name': 'TXT_INITVALUES_CATEGORY_GARDEN'
}, {
  '_id': '58bfc0fcb150e07f1374d1c5',
  'parent_id': '58bfc0fcb150e07f1374d1bb',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010801',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.965Z',
  'name': 'TXT_INITVALUES_CATEGORY_UNIVERSITY_COLLEGE'
}, {
  '_id': '58bfc0fcb150e07f1374d1c6',
  'parent_id': '58bfc0fcb150e07f1374d1bd',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000011003',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.967Z',
  'name': 'TXT_INITVALUES_CATEGORY_TAX'
}, {
  '_id': '58bfc0fcb150e07f1374d1c7',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010601',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.968Z',
  'name': 'TXT_INITVALUES_CATEGORY_TOBACCO'
}, {
  '_id': '58bfc0fcb150e07f1374d1c8',
  'parent_id': '58bfc0fcb150e07f1374d1bc',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010201',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.970Z',
  'name': 'TXT_INITVALUES_CATEGORY_WASHING_CLEANING'
}, {
  '_id': '58bfc0fcb150e07f1374d1c9',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010605',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.987Z',
  'name': 'TXT_INITVALUES_CATEGORY_TOYS'
}, {
  '_id': '58bfc0fcb150e07f1374d1ca',
  'parent_id': '58bfc0fcb150e07f1374d1be',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010704',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.994Z',
  'name': 'TXT_INITVALUES_CATEGORY_DOCTOR'
}, {
  '_id': '58bfc0fcb150e07f1374d1cb',
  'parent_id': '58bfc0fcb150e07f1374d1be',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010701',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:48.995Z',
  'name': 'TXT_INITVALUES_CATEGORY_BEAUTY_PRODUCTS'
}, {
  '_id': '58bfc0fdb150e07f1374d1cc',
  'parent_id': '58bfc0fcb150e07f1374d1be',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010703',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.061Z',
  'name': 'TXT_INITVALUES_CATEGORY_MEDICINE_LIFESTYLE'
}, {
  '_id': '58bfc0fdb150e07f1374d1cd',
  'parent_id': '58bfc0fcb150e07f1374d1b1',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000020200',
  'class_name': 'category-investment-income',
  'color': '#FF9800',
  'root': '58bfc0fcb150e07f1374d1b1',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.308Z',
  'name': 'TXT_INITVALUES_CATEGORY_INVESTMENT_INCOME'
}, {
  '_id': '58bfc0fdb150e07f1374d1ce',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010900',
  'class_name': 'category-pets',
  'color': '#e17392',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.309Z',
  'name': 'TXT_INITVALUES_CATEGORY_PETS'
}, {
  '_id': '58bfc0fdb150e07f1374d1cf',
  'parent_id': '58bfc0fcb150e07f1374d1b0',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010500',
  'class_name': 'category-transportation',
  'color': '#d14949',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.310Z',
  'name': 'TXT_INITVALUES_CATEGORY_TRANSPORTATION'
}, {
  '_id': '58bfc0fdb150e07f1374d1d0',
  'parent_id': '58bfc0fcb150e07f1374d1b2',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000030300',
  'class_name': 'category-withdraw-deposit',
  'color': '#009688',
  'root': '58bfc0fcb150e07f1374d1b2',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.488Z',
  'name': 'TXT_INITVALUES_CATEGORY_DEPOSIT'
}, {
  '_id': '58bfc0fdb150e07f1374d1d1',
  'parent_id': '58bfc0fcb150e07f1374d1b2',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000030100',
  'class_name': 'category-withdrawal',
  'color': '#9C27B0',
  'root': '58bfc0fcb150e07f1374d1b2',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.547Z',
  'name': 'TXT_INITVALUES_CATEGORY_WITHDRAWAL'
}, {
  '_id': '58bfc0fdb150e07f1374d1d2',
  'parent_id': '58bfc0fcb150e07f1374d1b7',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010103',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.619Z',
  'name': 'TXT_INITVALUES_CATEGORY_UTILITIES'
}, {
  '_id': '58bfc0fdb150e07f1374d1d3',
  'parent_id': '58bfc0fcb150e07f1374d1b7',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010101',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.625Z',
  'name': 'TXT_INITVALUES_CATEGORY_HOUSING'
}, {
  '_id': '58bfc0fdb150e07f1374d1d4',
  'parent_id': '58bfc0fcb150e07f1374d1b7',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010104',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.626Z',
  'name': 'TXT_INITVALUES_CATEGORY_TELECOMMUNICATION'
}, {
  '_id': '58bfc0fdb150e07f1374d1d5',
  'parent_id': '58bfc0fcb150e07f1374d1bb',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010802',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.750Z',
  'name': 'TXT_INITVALUES_CATEGORY_LANGUAGE_SCHOOL'
}, {
  '_id': '58bfc0fdb150e07f1374d1d6',
  'parent_id': '58bfc0fcb150e07f1374d1bd',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000011004',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.751Z',
  'name': 'TXT_INITVALUES_CATEGORY_LOAN_EXPENSE'
}, {
  '_id': '58bfc0fdb150e07f1374d1d7',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010607',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.752Z',
  'name': 'TXT_INITVALUES_CATEGORY_CHARITY'
}, {
  '_id': '58bfc0fdb150e07f1374d1d8',
  'parent_id': '58bfc0fcb150e07f1374d1bc',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010202',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.753Z',
  'name': 'TXT_INITVALUES_CATEGORY_HOUSEHOLD_APPLIANCES'
}, {
  '_id': '58bfc0fdb150e07f1374d1d9',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010604',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.766Z',
  'name': 'TXT_INITVALUES_CATEGORY_FURNITURE'
}, {
  '_id': '58bfc0fdb150e07f1374d1da',
  'parent_id': '58bfc0fcb150e07f1374d1b9',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010403',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.772Z',
  'name': 'TXT_INITVALUES_CATEGORY_HOBBY'
}, {
  '_id': '58bfc0fdb150e07f1374d1db',
  'parent_id': '58bfc0fcb150e07f1374d1b9',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010402',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.852Z',
  'name': 'TXT_INITVALUES_CATEGORY_VACATION'
}, {
  '_id': '58bfc0fdb150e07f1374d1dc',
  'parent_id': '58bfc0fcb150e07f1374d1b9',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010401',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.856Z',
  'name': 'TXT_INITVALUES_CATEGORY_GOING_OUT'
}, {
  '_id': '58bfc0fdb150e07f1374d1dd',
  'parent_id': '58bfc0fdb150e07f1374d1cf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010504',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:49.927Z',
  'name': 'TXT_INITVALUES_CATEGORY_TAXI'
}, {
  '_id': '58bfc0feb150e07f1374d1de',
  'parent_id': '58bfc0fcb150e07f1374d1bc',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010203',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.006Z',
  'name': 'TXT_INITVALUES_CATEGORY_OFFICE_SUPPLIES'
}, {
  '_id': '58bfc0feb150e07f1374d1df',
  'parent_id': '58bfc0fcb150e07f1374d1b8',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010302',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.013Z',
  'name': 'TXT_INITVALUES_CATEGORY_GROCERIES'
}, {
  '_id': '58bfc0feb150e07f1374d1e0',
  'parent_id': '58bfc0fdb150e07f1374d1cf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010501',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.188Z',
  'name': 'TXT_INITVALUES_CATEGORY_CAR'
}, {
  '_id': '58bfc0feb150e07f1374d1e1',
  'parent_id': '58bfc0fcb150e07f1374d1be',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010702',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.349Z',
  'name': 'TXT_INITVALUES_CATEGORY_WORKOUT'
}, {
  '_id': '58bfc0feb150e07f1374d1e2',
  'parent_id': '58bfc0fcb150e07f1374d1b2',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000030200',
  'class_name': 'category-savings',
  'color': '#3F51B5',
  'root': '58bfc0fcb150e07f1374d1b2',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.498Z',
  'name': 'TXT_INITVALUES_CATEGORY_SAVINGS'
}, {
  '_id': '58bfc0feb150e07f1374d1e3',
  'parent_id': '58bfc0fcb150e07f1374d1b8',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010301',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.687Z',
  'name': 'TXT_INITVALUES_CATEGORY_EATING_OUT'
}, {
  '_id': '58bfc0feb150e07f1374d1e4',
  'parent_id': '58bfc0fcb150e07f1374d1bd',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000011002',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.688Z',
  'name': 'TXT_INITVALUES_CATEGORY_INSURANCE'
}, {
  '_id': '58bfc0feb150e07f1374d1e5',
  'parent_id': '58bfc0fdb150e07f1374d1cf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010502',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.697Z',
  'name': 'TXT_INITVALUES_CATEGORY_FUEL'
}, {
  '_id': '58bfc0feb150e07f1374d1e6',
  'parent_id': '58bfc0fcb150e07f1374d1b7',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010102',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.698Z',
  'name': 'TXT_INITVALUES_CATEGORY_MAINTENANCE_COSTS'
}, {
  '_id': '58bfc0feb150e07f1374d1e7',
  'parent_id': '58bfc0fdb150e07f1374d1cf',
  'user': '58bfc0f9b150e07f1374d1ab',
  'global_category_id': '000000000000000000010503',
  'root': '58bfc0fcb150e07f1374d1b0',
  '__v': 0,
  'hidden': false,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-03-08T08:29:50.707Z',
  'name': 'TXT_INITVALUES_CATEGORY_PUBLIC_TRANSPORT'
}, {
  '_id': '593123640f49f10e00e05fc7',
  'user': '58bfc0f9b150e07f1374d1ab',
  'parent_id': '58bfc0fcb150e07f1374d1bf',
  'root': '58bfc0fcb150e07f1374d1b0',
  'class_name': '',
  'color': '',
  '__v': 0,
  'hidden': true,
  'deleted': false,
  'monthly_budget': 0,
  'service_codes': [],
  'created': '2017-06-02T08:35:48.007Z',
  'name': 'Állatpatika'
}];
