import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-standalone-control',
  templateUrl: './standalone-control.component.html',
  styleUrls: ['./standalone-control.component.scss']
})
export class StandaloneControlComponent implements OnInit {

  visibleComponent = 'TRANSACTIONS_TAB';

  constructor() { }

  ngOnInit() {
  }

  selectActiveTab(visibleActiveStandaloneTab) {
    this.visibleComponent = visibleActiveStandaloneTab;
  }
}
