import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StandaloneControlComponent } from './standalone-control.component';

describe('StandaloneControlComponent', () => {
  let component: StandaloneControlComponent;
  let fixture: ComponentFixture<StandaloneControlComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StandaloneControlComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StandaloneControlComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
