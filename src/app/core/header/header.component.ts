import { Component, OnInit } from '@angular/core';
import {LayoutActions} from '../../store/actions/layout.actions';
import {NgRedux, select} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  @select(['layout', 'sideNavIsOpen'])
  sideNavIsOpen: Observable<boolean>;

  constructor(private ngRedux: NgRedux<AppState>) { }

  ngOnInit() {
  }

  openSideNav() {
    this.ngRedux.dispatch({
      type: LayoutActions.TOGGLE_SIDENAV,
      payload: true
    });
  }
}
