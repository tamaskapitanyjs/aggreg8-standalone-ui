import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ControlComponent } from './control/control.component';
import { StandaloneControlComponent } from './control/standalone-control/standalone-control.component';
import {SharedModule} from '../shared/shared.module';
import {TransactionsModule} from '../transactions/transactions.module';
import { HeaderComponent } from './header/header.component';
import { SidenavComponent } from './sidenav/sidenav.component';
import {InfoSharingModule} from '../info-sharing/info-sharing.module';
import {EPIC_PROVIDERS, SERVICE_PROVIDERS} from '../store/epics';
import {StoreModule} from '../store/store.module';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {FileTranslateLoader} from './file.translate.loader';
import {ApiService} from './services/api.service';
import {HttpClientModule} from '@angular/common/http';
import {DialogModule} from '../dialog/dialog.module';
import {SyncModule} from '../sync/sync.module';

@NgModule({
  declarations: [
    ControlComponent,
    StandaloneControlComponent,
    HeaderComponent,
    SidenavComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TransactionsModule,
    InfoSharingModule,
    StoreModule,
    HttpClientModule,
    DialogModule,
    SyncModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: createTranslateLoader,
        deps: [ApiService]
      }
    })
  ],
  exports: [
    ControlComponent
  ],
  providers: [...EPIC_PROVIDERS, ...SERVICE_PROVIDERS]
})
export class CoreModule {}

let loader;
export function createTranslateLoader(api: ApiService) {
  if (!loader) {
    loader = new FileTranslateLoader(api);
  }
  return loader;
}
