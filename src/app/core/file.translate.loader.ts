import {Injectable} from '@angular/core';
import {TranslateLoader} from '@ngx-translate/core';
import {Observable} from 'rxjs';
import {ApiService} from './services/api.service';

@Injectable()
export class FileTranslateLoader implements TranslateLoader {
  translateObjectPath = '';

  constructor(private api: ApiService) {
  }

  setPathParam(path) {
    this.translateObjectPath = path;
  }

  getTranslation(lang: string): Observable<any> {
    return this.api.getTranslateJson(this.translateObjectPath, lang);
  }

  getPathParam() {
    return this.translateObjectPath;
  }
}
