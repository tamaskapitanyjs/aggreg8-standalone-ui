import {Component, OnInit} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {LayoutActions} from '../../store/actions/layout.actions';
import {Observable} from 'rxjs';
import {VisibleComponentElements, VisibleStandaloneUiTabs} from '../../store/state/layout.state';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss']
})
export class SidenavComponent implements OnInit {

  @select(['layout', 'sidenavClass'])
  sidenavClass: Observable<String>;
  @select(['model', 'userData'])
  userData: Observable<any>;

  visibleComponentsElement = VisibleComponentElements;
  defaultSidenavClass = 'sidenav opened-sidenav';

  constructor(private ngRedux: NgRedux<AppState>) {
  }

  ngOnInit() {
  }

  closeSideNav() {
    this.ngRedux.dispatch({
      type: LayoutActions.TOGGLE_SIDENAV,
      payload: false
    });
  }

  logout() {
    this.ngRedux.dispatch({type: LayoutActions.SIDENAV_CLASS, payload: 'sidenav closed-sidenav'});
    this.ngRedux.dispatch({type: LayoutActions.LOG_OUT});
  }

  editProfile() {
    this.ngRedux.dispatch({type: LayoutActions.SIDENAV_CLASS, payload: 'sidenav closed-sidenav'});
    this.ngRedux.dispatch({type: LayoutActions.SET_ACTIVE_TAB_IN_STANDALONE, payload: VisibleStandaloneUiTabs.SYNC_TAB});
    this.ngRedux.dispatch({type: LayoutActions.VISIBLE_COMPONENT, payload: VisibleComponentElements.EDIT_PROFILE});
  }
}
