import { Injectable } from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {combineLatest} from 'rxjs';
import {ApiService} from './api.service';

@Injectable({
  providedIn: 'root'
})
export class ModelService {

  constructor(private ngRedux: NgRedux<AppState>,
              private api: ApiService) { }

  public getTransactionsAndCategories() {
    return combineLatest([
      this.api.getTransactions(),
      this.api.getCategories(),
    ]);
  }
}
