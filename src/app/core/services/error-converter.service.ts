import {Injectable} from '@angular/core';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../store/root.state';

@Injectable({
  providedIn: 'root'
})
export class ErrorConverterService {

  constructor(private ngRedux: NgRedux<AppState>) {
  }

  convertErrorToTranslate(error) {
    if (typeof (error) === 'object' && error._body) {
      const jsonError = JSON.parse(error._body);
      if (jsonError.message) {
        const errorMessage = JSON.stringify(jsonError.message).replace(/"/g, '');
        if (errorMessage.includes('TXT')) {
          console.log('error message includes TXT');
          return errorMessage;
        }
      }
    }
    return 'TXT_ERROR_TEMPORARY_ERROR_OCCURRED';
  }
}
