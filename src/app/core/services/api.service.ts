import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders, HttpParams, HttpResponse} from '@angular/common/http';
import {NgRedux} from '@angular-redux/store';
import {AppState} from '../../store/root.state';
import {map} from 'rxjs/operators';
import * as models from '../../store/state/model.state';
import {mockTransactions} from '../../transactions/transactions.mock.data';
import {mockCategories} from '../../transactions/categories.mock.data';
import set = Reflect.set;
import {mockUserData} from '../user.mock.data';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  protected basePath = '';

  constructor(protected http: HttpClient,
              protected httpClient: HttpClient,
              private ngRedux: NgRedux<AppState>) {
  }

  public generateAuthUri(bankId: string, redirectUri: object) {
    return this.httpPost(`/banks/${bankId}/auth-uri`, redirectUri);
  }

  public postConsent(content: any): Observable<any> {
    return this.httpPost('/bank-access-consents', content);
  }

  public getConsents(): Observable<any> {
    return this.httpGet('/bank-access-consents');
  }

  public updateConsent(id: string, updatedConsent: any) {
    return this.httpPut(`/bank-access-consents/${id}`, updatedConsent);
  }

  public getConsentById(id: string): Observable<models.BankAccessConsent> {
    return this.httpGet(`/bank-access-consents/${id}`);
  }

  public getUserData(): Observable<any> {
    // return this.httpGet(`/users/me`);
    return new Observable((observer) => {
      setTimeout(() => {
        observer.next(mockUserData);
      }, 100);
    });
  }

  public getCustomerByNAme(name: string): Observable<models.BankAccessConsent> {
    return this.httpGet(`/customers/${name}`);
  }

  public postConsentedAccountsByConsentId(id: string, content: any): Observable<any> {
    return this.httpPost(`/bank-access-consents/${id}/consented-accounts`, content);
  }

  public postAuthorizeBySyncToken(token: any): Observable<any> {
    return this.httpPost(`/authorizeBySyncToken`, token);
  }

  public getTransactions(): Observable<any> {
    // return this.httpGet(`/transactions`);
    return new Observable((observer) => {
      setTimeout(() => {
        observer.next(mockTransactions);
      }, 100);
    });
  }

  public getCategories(): Observable<any> {
    // return this.httpGet(`/categories`);
    return new Observable((observer) => {
      setTimeout(() => {
        observer.next(mockCategories);
      }, 100);
    });
  }

  public getTranslateJson(path, lang): Observable<any> {
    return this.httpClient.get(path + '/i18n/' + lang + '.json');
  }

  private httpGet(path: string, queryParams?: HttpParams): Observable<any> {
    return this.httpRequest(path, 'Get', true, null, null, queryParams);
  }

  private httpPost(path: string, body?: any): Observable<any> {
    return this.httpRequest(path, 'Post', true, null, body);
  }

  private httpPut(path: string, body?: any): Observable<any> {
    return this.httpRequest(path, 'Put', true, null, body);
  }

  private httpDelete(path: string): Observable<any> {
    return this.httpRequest(path, 'Delete', false);
  }

  private httpRequest(path: string,
                      method: string,
                      jsonResponse: boolean,
                      headers?: HttpHeaders,
                      body?: any,
                      queryParams?: HttpParams): Observable<any> {
    if (!path.startsWith('/')) {
      path = '/' + path;
    }
    const url = this.basePath + path;
    let requestHeaders;
    if (headers) {
      requestHeaders = headers;
    } else {
      requestHeaders = this.defaultHeaders();
    }
    let requestBody;
    if (body) {
      requestBody = JSON.stringify(body);
    }
    const requestOptions = {
      headers: requestHeaders,
      body: requestBody,
      withCredentials: true,
      params: queryParams
    };

    return this.http.request(method, url, requestOptions).pipe(map((response: HttpResponse<any>) => {
      if (response.status === 204) {
        return undefined;
      }
      if (jsonResponse) {
        return response;
      }
      return {};
    }));
  }

  private defaultHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    const sessionToken = this.ngRedux.getState().model.sessionToken;
    headers = headers.append('Content-Type', 'application/json');
    if (sessionToken) {
      headers = headers.append('User-ApiKey', sessionToken);
    }
    return headers;
  }
}
