import { TestBed } from '@angular/core/testing';

import { ErrorConverterService } from './error-converter.service';

describe('ErrorConverterService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ErrorConverterService = TestBed.get(ErrorConverterService);
    expect(service).toBeTruthy();
  });
});
